#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <M5Stack.h>
#include <WiFi.h>
#include "time.h"
#include "TFTTerminal.h"

#include "phonebook.h" // Phonebook of allowed numbers

// TOPBAR PARAMETERS
#define TOPBAR_CLOCK_X 2
#define TOPBAR_CLOCK_Y 5
#define TOPBAR_GSM_STATUS_X 160
#define TOPBAR_GSM_STATUS_Y 5
#define TOPBAR_RSSI_X 270
#define TOPBAR_RSSI_Y 5
#define TOPBAR_NETWORK_PROVIDER_X 160
#define TOPBAR_NETWORK_PROVIDER_Y 21
#define TOPBAR_OWN_NUMBER_X 2
#define TOPBAR_OWN_NUMBER_Y 21
#define TOPBAR_BACKGROUND_NORMAL TFT_NAVY
#define TOPBAR_BACKGROUND_CALL TFT_DARKGREEN
#define TOPBAR_BACKGROUND_DENIED TFT_RED

// GSM PARAMETERS
#define GSM_RX_BUFFER_SIZE 128
#define GSM_QUERY_INTERVAL 5
#define GSM_RINGING_TIMEOUT_TIME 5 // How many seconds waiting after "ringing" before forced "no carrier".
#define GSM_RESPONSE_TIMEOUT_TIME 10 // How many seconds waiting until status changed to "unknown" if no response from modem.

// RELAY GPIO PORT
#define RELAY_GPIO_PORT 21

const char* pin_code   = "1234"; // ADD REAL SIM CARD PIN CODE HERE BEFORE FLASHING!!!

// Amount of phonenumbers in phonebook
int phonebook_count;

enum gsm_query
{
  Signal_quality_report_exe,
  Network_registration_read,
  Enter_PIN_read,
  Clock_read,
  Own_number,
  Network_provider
};

enum gsm_status
{
  Not_active,
  Connected,
  Searching,
  Denied,
  Roaming = 5
};

enum pin_status
{
  SIM_READY,
  SIM_PIN,
  SIM_PUK,
  PH_SIM_PIN,
  PH_SIM_PUK,
  SIM_PIN2,
  SIM_PUK2,
  SIM_UNKNOWN
};

enum gsm_exe_commands
{
  GSM_EXE_NONE,
  GSM_EXE_SET_PIN,
  GSM_EXE_ENABLE_CLIP,
  GSM_EXE_ENABLE_AUTOUPDATE,
  GSM_EXE_SAVE_SETTINGS,
  GSM_EXE_WAITING=99
};

struct TopBarInfo
{
  char gsm_status[20];
  char rssi[10];
  char network_provider[32];
  char own_number[32];
} top_bar_data;

// Top Info Bar
TFT_eSprite topBar = TFT_eSprite(&M5.Lcd);
uint16_t topbar_bg_color = TOPBAR_BACKGROUND_NORMAL;
uint16_t topbar_txt_color = TFT_WHITE;

// Use m5 TFT display as running terminal
TFT_eSprite TerminalBuffer = TFT_eSprite(&M5.Lcd);
TFTTerminal terminal(&TerminalBuffer);
uint16_t bg_color = TFT_BLACK;
uint16_t txt_color = TFT_WHITE;

// GSM variables
int gsm_exe_command = GSM_EXE_NONE; // Set this based on "gsm exe commands" if willing to send a command.
int ringing_timeout = 0; // Incoming ringing sets this and it is recuded every second.
int access_ok = 0; // 0 = idle, 1 = access ok, 2 = access denied.
int gsm_response_timeout = 0; // Modem response timeout variable.
int gsm_reset_timeout = 0; // Reset timeout counter

boolean* exe_target;

boolean clip_state = false;
boolean pin_state = true;
boolean auto_update_state = false;
boolean save_settings_now = false;

// Interrupt (ISR) variables
volatile int sec_interruptCounter;
hw_timer_t *sec_timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;


// Interrupt call
void IRAM_ATTR onSecTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  sec_interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}


void setRelay(boolean state)
{
  if(state)
  {
    digitalWrite(RELAY_GPIO_PORT, HIGH);
  }
  else
  {
    digitalWrite(RELAY_GPIO_PORT, LOW);
  }
}


void updateTime(char* timechar)
{
  int y, mo, d, h, mi, s, tz;
  char temp_ch[5];

  timechar[3] = '\0';
  timechar[6] = '\0';
  timechar[9] = '\0';
  timechar[12] = '\0';
  timechar[15] = '\0';
  timechar[18] = '\0';
  timechar[21] = '\0';

  y = atoi(&timechar[1]);
  mo = atoi(&timechar[4]);
  d = atoi(&timechar[7]);
  h = atoi(&timechar[10]);
  mi = atoi(&timechar[13]);
  s = atoi(&timechar[16]);
  tz = atoi(&timechar[19]);
  
  struct tm new_time;
  
  new_time.tm_hour = h;
  new_time.tm_min = mi;
  new_time.tm_sec = s;
  new_time.tm_mday = d;
  new_time.tm_mon = mo-1;
  new_time.tm_year = (y+100);

  time_t epoch = mktime(&new_time);
  struct timeval now_tv;
  now_tv.tv_sec = epoch;
  now_tv.tv_usec = 0;
  settimeofday(&now_tv, NULL);
}


void updateTopBar()
{
  char tm_str[24];

  time_t my_time = time(NULL);
  strftime(tm_str, 23,"%Y-%m-%d %H:%M:%S", localtime(&my_time));
  topBar.fillSprite(topbar_bg_color);
  topBar.setTextColor(topbar_txt_color);
  topBar.setCursor(TOPBAR_CLOCK_X,TOPBAR_CLOCK_Y);
  topBar.print(tm_str);

  topBar.setCursor(TOPBAR_GSM_STATUS_X, TOPBAR_GSM_STATUS_Y);
  topBar.print(top_bar_data.gsm_status);

  topBar.setCursor(TOPBAR_RSSI_X, TOPBAR_RSSI_Y);
  topBar.print(top_bar_data.rssi);

  topBar.setCursor(TOPBAR_OWN_NUMBER_X, TOPBAR_OWN_NUMBER_Y);
  topBar.print(top_bar_data.own_number);

  topBar.setCursor(TOPBAR_NETWORK_PROVIDER_X, TOPBAR_NETWORK_PROVIDER_Y);
  topBar.print(top_bar_data.network_provider);

  topBar.pushSprite(0,0);
}

void printTerminal(const char* format, ...)
{
  char tm_str[28];
  char txt_str[63];
  va_list args;
  
  time_t my_time = time(NULL);
  strftime(tm_str, 27,"<%Y-%m-%d %H:%M:%S>: ", localtime(&my_time));
  terminal.print(tm_str);

  va_start(args, format);
  vsprintf(txt_str, format, args);
  va_end(args);
  
  terminal.print(txt_str);
  terminal.print("\n");
}


boolean inPhoneBook(char* incoming)
{
  for (int i=0; i < phonebook_count; i++)
  {
    if (phonebook[i].equals(incoming))
    {
      return true;
    }
  }
  return false;
}


void parseGSM(char* parse_msg)
{
  char* resp;
  
  gsm_response_timeout = GSM_RESPONSE_TIMEOUT_TIME; // Reset gsm response timeout timer
  
  /** RSSI **/
  resp = strstr(parse_msg, "+CSQ:");
  if (resp)
  {
    char rssi_char[5];

    char* start_ptr = strstr(resp, " ");
    char* end_ptr = strstr(resp, ",");

    for (char* i=start_ptr; i < end_ptr; i++)
    {
      rssi_char[i-start_ptr] = *i;
      rssi_char[i-start_ptr+1] = '\0';
    }
    
    int rssi_val = atoi(rssi_char);
    int rssi_db = 0;
    
    if (rssi_val == 0)
    {
      rssi_db = -115;
    }
    else if (rssi_val == 1)
    {
      rssi_db = -111;
    }
    else if (rssi_val < 31)
    {
      rssi_db = -110 + ((rssi_val - 2) * 2);
    }
    else if (rssi_val == 31)
    {
      rssi_db = -52;
    }
    else
    {
      rssi_db = 0;
    }

    if (rssi_db == 0)
    {
      printTerminal("[PR] RSSI: unknown");
      sprintf(top_bar_data.rssi, "- dB");
    }
    else
    {
      printTerminal("[PR] RSSI: %d dB", rssi_db);
      sprintf(top_bar_data.rssi, "%d dB", rssi_db);
    }
    return;
  }

  /** Network registration **/
  resp = strstr(parse_msg, "+CREG:");
  if (resp)
  {
    char* start_ptr = strstr(resp, ",");
    start_ptr++;
    
    int status_val = atoi(start_ptr);
    switch(status_val)
    {
      case(Not_active): // Not registered, not searching.
        printTerminal("[PR] Network: Not active");
        sprintf(top_bar_data.gsm_status, "Not active");
        break;

      case(Connected): // Registered, home network.
        printTerminal("[PR] Network: Connected");
        sprintf(top_bar_data.gsm_status, "Connected");
        break;

      case(Searching): // Not registered, searching
        printTerminal("[PR] Network: Searching");
        sprintf(top_bar_data.gsm_status, "Searching");
        break;

      case(Denied): // Registration denied
        printTerminal("[PR] Network: Denied");
        sprintf(top_bar_data.gsm_status, "Denied");
        break;

      case(Roaming): // Registered, roaming network.
        printTerminal("[PR] Network: Roaming");
        sprintf(top_bar_data.gsm_status, "Roaming");
        break;

      default: // Unknown
        printTerminal("[PR] Network: Unknown [%d].", status_val);
        sprintf(top_bar_data.gsm_status, "Unknown");
        break;
    }
    return;
  }

  /** PIN **/
  resp = strstr(parse_msg, "+CPIN:");
  if (resp)
  {
    char* start_ptr = strstr(resp, " ");
    start_ptr++;

    int pin_state_val;
    if (strcmp(start_ptr, "READY") == 0)
    {
      pin_state_val = SIM_READY;
      printTerminal("[PR] PIN Ready");
    }
    else if (strcmp(start_ptr, "SIM PIN") == 0)
    {
      pin_state_val = SIM_PIN;
      printTerminal("[PR] SIM PIN");
      pin_state = false; // Force to send pin code
    }
    else if (strcmp(start_ptr, "SIM PUK") == 0)
    {
      pin_state_val = SIM_PUK;
      printTerminal("[PR] SIM PUK");
    }
    else if (strcmp(start_ptr, "PH_SIM PIN") == 0)
    {
      pin_state_val = PH_SIM_PIN;
      printTerminal("[PR] PH SIM PIN");
    }
    else if (strcmp(start_ptr, "PH_SIM PUK") == 0)
    {
      pin_state_val = PH_SIM_PUK;
      printTerminal("[PR] PH SIM PUK");
    }
    else if (strcmp(start_ptr, "SIM PIN2") == 0)
    {
      pin_state_val = SIM_PIN2;
      printTerminal("[PR] SIM PIN2");
    }
    else if (strcmp(start_ptr, "SIM PUK2") == 0)
    {
      pin_state_val = SIM_PUK2;
      printTerminal("[PR] SIM PUK2");
    }
    else
    {
      pin_state_val = SIM_UNKNOWN;
      printTerminal("[PR] ?? %s", start_ptr);
    }
    return;
  }

  /** OWN NUMBER **/
  resp = strstr(parse_msg, "+CNUM:");
  if (resp)
  {
    char* start_ptr = strstr(resp, ",");
    start_ptr += 2;
    char* end_ptr = strstr(start_ptr,",");
    end_ptr--;
    end_ptr[0] = '\0';
    sprintf(top_bar_data.own_number, "[%s]", start_ptr);
    return;
  }

  /** NETWORK PROVIDER **/
  resp = strstr(parse_msg, "+CSPN:");
  if (resp)
  {
    printTerminal("[PR] %s", resp);
    char* start_ptr = strstr(resp, "\"");
    start_ptr++;
    char* end_ptr = strstr(start_ptr, "\"");
    end_ptr[0] = '\0';
    sprintf(top_bar_data.network_provider, "%s", start_ptr);
    return;
  }

  /** CLOCK **/
  resp = strstr(parse_msg, "+CCLK:");
  if (resp)
  {
    char* start_ptr = strstr(resp, "\"");
    updateTime(start_ptr);
    return;
  }

  /** INCOMING CALL **/
  resp = strstr(parse_msg, "RING");
  if (resp)
  {
    ringing_timeout = GSM_RINGING_TIMEOUT_TIME;
    printTerminal("[PR] RING");
    return;
  }

  /** CALL HANG UP **/
  resp = strstr(parse_msg, "NO CARRIER");
  if (resp)
  {
    ringing_timeout = 0;
    access_ok = 0;
    printTerminal("[PR] NO CARRIER");
    return;
  }

  /** CALLER INFO **/
  resp = strstr(parse_msg, "+CLIP:");
  if (resp)
  {
    char number_char[30];

    char* start_ptr = strstr(resp, " ");
    start_ptr+=2;
    char* end_ptr = strstr(resp, ",");
    end_ptr--;

    for (char* i=start_ptr; i < end_ptr; i++)
    {
      number_char[i-start_ptr] = *i;
      number_char[i-start_ptr+1] = '\0';
    }

    if (inPhoneBook(number_char))
    {
      printTerminal("[PR] %s OK", number_char);
      access_ok = 1;
    }
    else
    {
      printTerminal("[PR] %s DENIED", number_char);
      access_ok = 2;
    }
    
    return;
  }

  if (gsm_exe_command == GSM_EXE_WAITING)
  {
    /** OK RESPONSE **/
    resp = strstr(parse_msg, "OK");
    if (resp)
    {
      *exe_target = !*exe_target;
      gsm_exe_command = GSM_EXE_NONE;
      return;
    }
  
    /** ERROR RESPONSE **/
    resp = strstr(parse_msg, "ERROR");
    if (resp)
    {
      gsm_exe_command = GSM_EXE_NONE;
      return;
    }
  }
}


void readGSM()
{
  static char rx_buffer[GSM_RX_BUFFER_SIZE];
  static uint8_t buf_pos = 0;

  char temp_rx = Serial2.read();
  if (buf_pos < (GSM_RX_BUFFER_SIZE - 1))
  {
    if (temp_rx == '\r') // Message termination received
    {
      printTerminal("[RX] %s", rx_buffer);
      parseGSM(rx_buffer);
      
      buf_pos = 0;
      rx_buffer[buf_pos] = '\0';
    }
    else if (temp_rx == '\n') // No reaction to line feed
    {
      return;
    }
    else // Any other chararcter is saved.
    {
      rx_buffer[buf_pos++] = temp_rx;
      rx_buffer[buf_pos] = '\0';
    }
  }
  else // Message buffer full, no termination received
  {
    printTerminal("[RX] %s", rx_buffer);
    parseGSM(rx_buffer);
    
    buf_pos = 0;
    rx_buffer[buf_pos] = '\0';
  }
}


void setup()
{    
  M5.begin();
  
  pinMode(RELAY_GPIO_PORT, OUTPUT); // Relay control PIN  
  setRelay(false);
     
  Serial.begin(115200);

  // GSM module connection
  Serial2.begin(115200, SERIAL_8N1, 5, 13);

  // Timer for ISR
  sec_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(sec_timer, &onSecTimer, true);
  timerAlarmWrite(sec_timer, 1000000, true);
  timerAlarmEnable(sec_timer);

  // TopBar Sprite
  topBar.setColorDepth(8);
  topBar.createSprite(320,32);
  topBar.fillSprite(topbar_bg_color);
  topBar.pushSprite(0,0);

  // Background of the terminal section
  TerminalBuffer.setColorDepth(8);
  TerminalBuffer.createSprite(320,204);
  TerminalBuffer.fillSprite(bg_color);
  TerminalBuffer.pushSprite(0,33);  
  
  // Actual terminal section
  terminal.setGeometry(0,36,320,204);
  terminal.setcolor(txt_color, bg_color);

  // Count numbers in phonebook
  phonebook_count = sizeof(phonebook)/sizeof(phonebook[0]);

  // Update TopBar
  updateTopBar();
}

void loop()
{
  static String gsm_str = "";
  static int gsm_query_counter = 0;
  static int gsm_query_msg_counter = 0;
  static int gsm_exe_waiting_counter = 0;

  // updateTopBar at first to keep seconds running smoothly
  updateTopBar();

  // LOGIC FOR EXECUTION COMMANDS
  if (gsm_exe_command == GSM_EXE_NONE)
  {
    if (save_settings_now == true)
    {
      exe_target = &save_settings_now;
      gsm_exe_command = GSM_EXE_SAVE_SETTINGS;
    }
    else if (pin_state == false)
    {
      exe_target = &pin_state;
      gsm_exe_command = GSM_EXE_SET_PIN;
    }
    else if (clip_state == false)
    {
      exe_target = &clip_state;
      gsm_exe_command = GSM_EXE_ENABLE_CLIP;
    }
    else if (auto_update_state == false)
    {
      exe_target = &auto_update_state;
      gsm_exe_command = GSM_EXE_ENABLE_AUTOUPDATE;
      save_settings_now = true; // Save settings to permanent memory
    }
  }

  // GSM buffer check
  if (Serial2.available())
  {
    readGSM();
  }

  // Execute once a sec via ISR
  if (sec_interruptCounter > 0) {

    // Zero interrupt counter first
    portENTER_CRITICAL(&timerMux);
    sec_interruptCounter = 0;
    portEXIT_CRITICAL(&timerMux);
 
    /** Interrupt handling code **/

    if (ringing_timeout > 0)
    {
      switch(access_ok)
      {
        case(0):
          topbar_bg_color = TOPBAR_BACKGROUND_NORMAL;
          setRelay(false);
          break;
        case(1):
          topbar_bg_color = TOPBAR_BACKGROUND_CALL;
          setRelay(true);
          break;
        case(2):
          topbar_bg_color = TOPBAR_BACKGROUND_DENIED;
          setRelay(false);
          break;
        default:
          topbar_bg_color = TOPBAR_BACKGROUND_NORMAL;
          setRelay(false);
          break;
      }
      
      ringing_timeout--;
      if (ringing_timeout == 0)
      {
        access_ok = 0;
      }
    }
    else if (gsm_exe_command != GSM_EXE_NONE)
    {
      topbar_bg_color = TOPBAR_BACKGROUND_NORMAL;
      setRelay(false);     
      
      switch(gsm_exe_command)
      {
        case(GSM_EXE_SET_PIN):
          char temp_cmd[50];
          sprintf(temp_cmd , "AT+CPIN=%s\r\n", pin_code);
          Serial2.print(temp_cmd);
          printTerminal("[TX] AT+CPIN=%s", pin_code);
          gsm_exe_command = GSM_EXE_WAITING;
          gsm_exe_waiting_counter = GSM_RESPONSE_TIMEOUT_TIME;
          break;

        case(GSM_EXE_ENABLE_CLIP):
          Serial2.print("AT+CLIP=1\r\n");
          printTerminal("[TX] AT+CLIP=1");
          gsm_exe_command = GSM_EXE_WAITING;
          gsm_exe_waiting_counter = GSM_RESPONSE_TIMEOUT_TIME;
          break;

        case(GSM_EXE_ENABLE_AUTOUPDATE):
          Serial2.print("AT+CLTS=1\r\n"); // Enable auto time update from network
          printTerminal("[TX] AT+CLTS=1");
          gsm_exe_command = GSM_EXE_WAITING;
          gsm_exe_waiting_counter = GSM_RESPONSE_TIMEOUT_TIME;
          break;

        case(GSM_EXE_SAVE_SETTINGS):
          Serial2.print("AT&W\r\n"); // Save settings
          printTerminal("[TX] AT&W");
          gsm_exe_command = GSM_EXE_WAITING;
          gsm_exe_waiting_counter = GSM_RESPONSE_TIMEOUT_TIME;
          break;
          
        default:
          break;
      }
      if(gsm_exe_waiting_counter > 0)
      {
        gsm_exe_waiting_counter--;
        if (gsm_exe_waiting_counter == 0)
        {
          gsm_exe_command = GSM_EXE_NONE;
        }
      }
    }
    else
    {
      topbar_bg_color = TOPBAR_BACKGROUND_NORMAL;
      setRelay(false);
      
      gsm_query_counter++;
      if(gsm_query_counter == GSM_QUERY_INTERVAL)
      {
        switch(gsm_query_msg_counter)
        {
          case(Signal_quality_report_exe):
            Serial2.print("AT+CSQ\r\n");
            printTerminal("[TX] AT+CSQ");
            gsm_query_msg_counter++;
            break;
          case(Network_registration_read):
            Serial2.print("AT+CREG?\r\n");
            printTerminal("[TX] AT+CREG?");
            gsm_query_msg_counter++;
            break;
          case(Enter_PIN_read):
            Serial2.print("AT+CPIN?\r\n");
            printTerminal("[TX] AT+CPIN?");
            gsm_query_msg_counter++;
            break;
          case(Clock_read):
            Serial2.print("AT+CCLK?\r\n");
            printTerminal("[TX] AT+CCLK?");
            gsm_query_msg_counter++;
            break;
          case(Own_number):
            Serial2.print("AT+CNUM\r\n");
            printTerminal("[TX] AT+CNUM");
            gsm_query_msg_counter++;
            break;
          case(Network_provider):
            Serial2.print("AT+CSPN?\r\n");
            printTerminal("[TX] AT+CSPN?\r\n");
            gsm_query_msg_counter = 0;
          default:
            gsm_query_msg_counter = 0;
            break;
        }
        gsm_query_counter = 0;
      }
    }

    if (gsm_response_timeout > 0)
    {
      if (gsm_response_timeout == 1)
      {
        sprintf(top_bar_data.gsm_status, "No response");
        sprintf(top_bar_data.rssi, "(-) dB");
        sprintf(top_bar_data.network_provider, "");
        // sprintf(top_bar_data.own_number, "");  //Don't remove own number if connection lost..
      }
      gsm_response_timeout--;
    }
  }
}
